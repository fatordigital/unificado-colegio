<?php
$acao=mysql_query("select * from proposta_pedagogica where id='4'") or die (mysql_error());
while($r=mysql_fetch_array($acao)) 
    {
    $rec_titulo=utf8_encode($r['titulo']);
    $rec_subtitulo=utf8_encode($r['subtitulo']);
    $rec_imagem=$r['imagem'];
    $rec_texto=utf8_encode($r['texto']);
	
	$rec_texto = str_replace("<hr />","<div style=\"padding-top:20px; margin-bottom:20px; border-bottom:1px dotted rgba(0,0,0,.3)\"></div>",$rec_texto);
	$rec_texto = str_replace("<h2 class=\"title section-title\">","<h2>",$rec_texto);
	$rec_texto = str_replace("<h3>","<h4>",$rec_texto);
	$rec_texto = str_replace("</h3>","</h4>",$rec_texto);
	$rec_texto = str_replace("<table border=\"1\" cellpadding=\"1\" cellspacing=\"1\" style=\"width:500px\">","<div class=\"table_container table-responsive\"><table class=\"table table-hover\">",$rec_texto);
	$rec_texto = str_replace("</table>","</table></div>",$rec_texto);
	$rec_texto = str_replace("<thead>","<thead style=\"background:#333; color:#fff\">",$rec_texto);
	}
?>
<!doctype html>
<!--[if lt IE 7 ]> <html class="ie ie6 ie-lt10 ie-lt9 ie-lt8 ie-lt7 no-js" lang="en"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie ie7 ie-lt10 ie-lt9 ie-lt8 no-js" lang="en"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie ie8 ie-lt10 ie-lt9 no-js" lang="en"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie ie9 ie-lt10 no-js" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--><html class="no-js" lang="pt"><!--<![endif]-->
<!-- the "no-js" class is for Modernizr. --> 
<head>
	
	<!-- Important stuff for SEO, don't neglect. (And don't dupicate values across your site!) -->
	<title>Projeto Pedagógico</title>
	
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />    
    <meta itemprop="name" content="Unificado Z">
	<meta name="title" content="Curso Revisão" />
	<meta http-equiv="content-language" content="pt-br" />
	<meta http-equiv="cache-control" content="no-cache" />
	<meta http-equiv="pragma" content="no-store" />
	<meta http-equiv="refresh" content="none" />
	<meta name="reply-to" content="contato@unificado.com.br">
	<meta name="generator" content="Adobe Dreamweaver Macromedia 6.0">
    <meta itemprop="description" content="As escolhas sempre quando bem feitas, resultam em grandes conquistas. O Unificado Z teve seu início com o antigo Pré-Vestibular, em 1977, formado por um grupo de professores que acreditaram na ideia de oferecer um ensino baseado em um método pedagógico diferente.">
    <meta name="keywords" content="plano empresarial claro, plano corporativo claro, plano claro empresarial, plano claro corporativo, plano empresa, plano corporativo, plano empresarial de telefonia, plano corporativo de telefonia" />
    <meta itemprop="image" content="http://www.unificadoz.com.br/img/meta-imagem.jpg">
	<meta name="abstract" content="As escolhas sempre quando bem feitas, resultam em grandes conquistas. O Unificado Z teve seu início com o antigo Pré-Vestibular, em 1977, formado por um grupo de professores que acreditaram na ideia de oferecer um ensino baseado em um método pedagógico diferente.">    
	<meta name="author" content="WE MAKE | Marketing Digital" />
	<meta name="robots" content="index, follow" />
	<meta name="rating" content="general" />
	<meta name="copyright" content="Copyright Grupo Unificado 2015. All Rights Reserved." />    
    <meta property="og:type" content="website" />
    <meta property="og:url" content="http://www.unificadoz.com.br" />
    <meta property="og:image" content="http://www.unificadoz.com.br/img/meta-imagem.jpg" />
    <meta property="og:title" content="Curso Revisão"/>
    <meta property="og:description" content="As escolhas sempre quando bem feitas, resultam em grandes conquistas. O Unificado Z teve seu início com o antigo Pré-Vestibular, em 1977, formado por um grupo de professores que acreditaram na ideia de oferecer um ensino baseado em um método pedagógico diferente.">
    <meta property="og:site_name" content="Unificado Z" />
    <meta property="og:author" content="WE MAKE Marketing Digital" />
	<meta name="Copyright" content="Grupo Unificado" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<!-- concatenate and minify for production -->
	<link rel="stylesheet" href="/css/style.css" type="text/css" media="all" />
	<link rel="stylesheet" href="/css/bootstrap.css" type="text/css" media="all" />
	<link rel="stylesheet" href="/css/icon-fonts.css" type="text/css" media="all" />
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    
</head>


<body class="preloader3">

<div id="main_wrapper">

	<?php include_once 'includes/header.php'; ?>
		
	<!-- Page Title -->

    <section class="content_section white_section page_title has_bg_image bg-curso-extensivo enar_parallax">

		<div class="content clearfix">

			<h1 class="">Projeto Pedagógico</h1>
			<div class="breadcrumbs">
				<a href="/">Home</a>
				<span class="crumbs-spacer"><i class="ico-angle-right"></i></span>
				<a href="/proposta">Proposta</a>
				<span class="crumbs-spacer"><i class="ico-angle-right"></i></span>
				<span>Projeto Pedagógico</span>
			</div>
		</div>

	</section>

	<!-- End Page Title -->

	
	<!-- Intro Banner -->
	<section class="content_section">
		<div class="container row_spacer2">
			<div class="container">
				<div class="content clearfix">
                    <div class="col-md-8">

    				<?php echo $rec_texto; ?>
                        
                	</div>
                    <div class="col-md-4" id="form">                    
					<?php 
					$action = "/proposta/projeto-pedagogico#form";
					include"includes/form-contato-lateral.php"; ?>
                    </div>
				</div>
			</div>
		</div>
	</section>
	<!-- End Intro Banner -->
	
	<?php include_once 'includes/footer.php'; ?>

	<a href="#0" class="hm_go_top"></a>
</div>
<!-- End wrapper -->

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="/js/jquery.js"><\/script>')</script>
<script type="text/javascript" src="/js/functions.js"></script>
<script src="/js/plugins.js"></script>

</body>
</html>