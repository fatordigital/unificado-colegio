					<!-- Social Media -->
					<div class="widget_block">
						<h6 class="widget_title">Redes Sociais</h6>
						<div class="social_links_widget clearfix">
							<a href="#" target="_blank" class="twitter"><i class="ico-twitter4"></i></a>
							<a href="#" target="_blank" class="facebook"><i class="ico-facebook4"></i></a>        
							<a href="#" target="_blank" class="googleplus"><i class="ico-google-plus"></i></a>     
							<a href="#" target="_blank" class="linkedin"><i class="ico-linkedin3"></i></a>
							<a href="#" target="_blank" class="vimeo"><i class="ico-vimeo"></i></a>
							
							<!-- <a href="skype:#?call" class="skype"><i class="ico-skype2"></i></a>
							<a href="#" target="_blank" class="rss"><i class="ico-rss"></i></a>
							<a href="#" target="_blank" class="flickr"><i class="ico-flickr2"></i></a>
							<a href="#" target="_blank" class="picasa"><i class="ico-picassa"></i></a>
							<a href="#" target="_blank" class="tumblr"><i class="ico-tumblr"></i></a>
							
							<a href="#" target="_blank" class="dribble"><i class="ico-dribbble"></i></a>
							<a href="#" target="_blank" class="soundcloud"><i class="ico-soundcloud"></i></a>
							<a href="#" target="_blank" class="instagram"><i class="ico-instagram3"></i></a>
							<a href="#" target="_blank" class="pinterest"><i class="ico-pinterest-p"></i></a>
							<a href="#" target="_blank" class="youtube"><i class="ico-youtube3"></i></a> -->
						</div>
					</div>
					<!-- End Social Media -->

					<!-- Tag Cloud
					<div class="widget_block">
						<h6 class="widget_title">Tag Cloud</h6>
						<div class="tagcloud style2 clearfix">
							<a href="#"><span class="tag">Vestibular</span><span class="num">12</span></a>
							<a href="#"><span class="tag">Cursinho</span><span class="num">24</span></a>
							<a href="#"><span class="tag">Unificado</span><span class="num">8</span></a>
							<a href="#"><span class="tag">Cursos</span><span class="num">12</span></a>
							<a href="#"><span class="tag">Consurcos</span><span class="num">4</span></a>
							<a href="#"><span class="tag">EJA</span><span class="num">56</span></a>
						</div>
					</div>
					End Tag Cloud -->				
					
					<!-- Categories -->
					<div class="widget_block">
						<h6 class="widget_title">Sobre o Colégio</h6>
						<ul class="cat_list_widget">
							<li>
								<a href="../colegio/quem-somos">Quem Somos</a>
							</li>
							<li>
								<a href="../colegio/historico">Histórico</a>
							</li>
							<li>
								<a href="../colegio/sedes">Sedes</a>
							</li>
							<li>
								<a href="http://www.unificado.com.br">Grupo Unificado</a>
							</li>
						</ul>
					</div>
					<!-- End Categories -->