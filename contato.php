<!doctype html>
<!--[if lt IE 7 ]> <html class="ie ie6 ie-lt10 ie-lt9 ie-lt8 ie-lt7 no-js" lang="en"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie ie7 ie-lt10 ie-lt9 ie-lt8 no-js" lang="en"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie ie8 ie-lt10 ie-lt9 no-js" lang="en"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie ie9 ie-lt10 no-js" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--><html class="no-js" lang="en"><!--<![endif]-->
<!-- the "no-js" class is for Modernizr. --> 
<head>

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	
	<!-- Important stuff for SEO, don't neglect. (And don't dupicate values across your site!) -->
	<title>Contato</title>
	<meta name="author" content="" />
	<meta name="description" content="" />
	
	<!-- Don't forget to set your site up: http://google.com/webmasters -->
	<meta name="google-site-verification" content="" />
	<meta name="Copyright" content="" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<!-- Use Iconifyer to generate all the favicons and touch icons you need: http://iconifier.net -->
	<link rel="shortcut icon" href="/images/favicon/favicon.ico" type="image/x-icon" />
	<link rel="apple-touch-icon" href="/images/favicon/apple-touch-icon.png" />
	<link rel="apple-touch-icon" sizes="57x57" href="/images/favicon/apple-touch-icon-57x57.png" />
	<link rel="apple-touch-icon" sizes="72x72" href="/images/favicon/apple-touch-icon-72x72.png" />
	<link rel="apple-touch-icon" sizes="76x76" href="/images/favicon/apple-touch-icon-76x76.png" />
	<link rel="apple-touch-icon" sizes="114x114" href="/images/favicon/apple-touch-icon-114x114.png" />
	<link rel="apple-touch-icon" sizes="120x120" href="/images/favicon/apple-touch-icon-120x120.png" />
	<link rel="apple-touch-icon" sizes="144x144" href="/images/favicon/apple-touch-icon-144x144.png" />
	<link rel="apple-touch-icon" sizes="152x152" href="/images/favicon/apple-touch-icon-152x152.png" />
	
	<!-- concatenate and minify for production -->
	<link rel="stylesheet" href="/css/style.css" type="text/css" media="all" />
	<link rel="stylesheet" href="/css/flexslider.css" type="text/css" media="all" />
	<link rel="stylesheet" href="/css/bootstrap.css" type="text/css" media="all" />
	<link rel="stylesheet" href="/css/animate.min.css" type="text/css" media="all" />
	
	<link rel="stylesheet" href="/css/magnific-popup.css" type="text/css" media="all" />
	<link rel="stylesheet" href="/css/icon-fonts.css" type="text/css" media="all" />

	<link href='http://fonts.googleapis.com/css?family=Oswald:400,700,300' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Lato:300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
</head>

<!-- Class ( site_boxed - dark - preloader1 - preloader2 - preloader3 - light_header - dark_sup_menu - menu_button_mode - transparent_header - header_on_side ) -->
<body class="preloader3">

<div id="main_wrapper">

	<?php include_once 'includes/header.php'; ?>
		
	<!-- Page Title -->

	<section class="content_section page_title">

		<div class="content clearfix">

			<h1 class="">Contato</h1>
			<div class="breadcrumbs">
				<a href="/">Home</a>
				<span class="crumbs-spacer"><i class="ico-angle-right"></i></span>
				<span>Contato</span>
			</div>
		</div>

	</section>

	<!-- End Page Title -->

	
	<!-- Contact Us -->
	<section class="content_section">
		<div class="content row_spacer no_padding">	
			<div class="rows_container clearfix">
				<div class="col-md-7">

					<?php if ($sucesso_contato == 1) { ?><h4 style="margin-top:0px; padding-top:0px; margin-bottom:60px;">Mensagem enviada com sucesso!</h4><?php } ?>
                    <?php if ($erro_form > 0) { ?><h4 style="margin-top:0px; padding-top:0px; margin-bottom:60px; color:red">Preencha os campos obrigatórios!</h4><?php } ?>
                
					<h2 class="title1 upper"><i class="ico-envelope3"></i>Envie uma Mensagem</h2>
                    <br><br>
					<form action="/contato" method="post" name="contato" id="contato"> 
						<div class="form_row clearfix">
							<label for="contact-us-name">
								<span class="hm_field_name"><?php if ($erro_nome == 1) { echo"<font style='color:red'>"; } ?>Nome<?php if ($erro_nome == 1) { echo"</font>"; } ?></span>
								<span class="hm_requires_star">*</span>
							</label>
							<input class="form_fill_fields hm_input_text" type="text" name="nome" id="nome" placeholder="Nome" value="<?php if ($erro_form == 1) { echo $nome; } ?>">
						</div>
						<div class="form_row clearfix">
							<label for="contact-us-mail">
								<span class="hm_field_name"><?php if (($erro_email == 1) or ($erro_valida_email == 1)) { echo"<font style='color:red'>"; } ?>Email<?php if (($erro_email == 1) or ($erro_valida_email == 1)) { echo"</font>"; } ?></span>
								<span class="hm_requires_star">*</span>
							</label>
							<input class="form_fill_fields hm_input_text" type="email" name="email" id="email" placeholder="mail@nomedosite.com" value="<?php if ($erro_form == 1) { echo $email; } ?>">
						</div>
						<div class="form_row clearfix">
							<label for="contact-us-phone">
								<span class="hm_field_name">Telefone</span>
							</label>
							<input class="form_fill_fields hm_input_text" type="text" name="telefone" id="telefone" placeholder="Telefone" value="<?php if ($erro_form == 1) { echo $telefone; } ?>">
						</div>
						<div class="form_row clearfix">
							<label for="contact-us-message">
								<span class="hm_field_name"><?php if ($erro_comentario == 1) { echo"<font style='color:red'>"; } ?>Comentários<?php if ($erro_comentario == 1) { echo"</font>"; } ?></span>
								<span class="hm_requires_star">*</span>
							</label>
							<textarea class="form_fill_fields hm_textarea" name="comentario" id="comentario" placeholder="Comentários"><?php if ($erro_form == 1) { echo $comentario; } ?></textarea>
						</div>
						<div class="form_row clearfix">
							<button type="submit" class="send_button full_button" name="contact-us-submit" id="contact-us-submit" value="submit">
								<i class="ico-check3"></i>
								<span>Enviar Mensagem</span>
							</button>
						</div>
                    <input type="hidden" name="enviar_contato" value="on">
					</form>

				</div><!-- Grid -->
				
				<div class="col-md-5">
					<div class="contact_details_row clearfix">
						<span class="icon">
							<i class="ico-location5"></i>
						</span>
						<div class="c_con">
							<span class="c_title">Endereço</span>
							<span class="c_detail">
								<span class="c_name">Matriz</span><br>
								<span class="c_desc"><?php echo $info_endereco; ?><br />
								<span class="c_desc"><?php echo $info_cidade; ?>, <?php echo $info_estado; ?></span>
							</span>
						</div>
					</div>
					
					<div class="contact_details_row clearfix">
						<span class="icon">
							<i class="ico-bubble4"></i>
						</span>
						<div class="c_con">
							<span class="c_title">Telefones</span>
							<span class="c_detail">
								<span class="c_name">Matriz</span><br>
								<span class="c_desc"><?php echo $info_telefone; ?></span>
							</span>
						</div>
					</div>
					
					<div class="contact_details_row clearfix">
						<span class="icon">
							<i class="ico-paperplane"></i>
						</span>
						<div class="c_con">
							<span class="c_title">Email</span>
							<span class="c_detail">
								<span class="c_name">Atendimento</span><br>
								<span class="c_desc"><?php echo $info_email; ?></span>
							</span>
						</div>
					</div>
                    
						<a href="/colegio/sedes" class="main_button" style="margin-left:80px">
							<i class="ico-angle-right"></i><span>Ver todas sedes</span>
						</a>

				</div>
			</div>
		</div>
	</section>
	<!-- End Contact Us -->
        
	<!-- Google Map -->
	<section class="content_section">
		<div class="title_banner t_b_color1 upper centered">
			<div class="content">
				<h2>Localização</h2>
			</div>
		</div>
		<div class="bordered_content">
			<div class="google_map" data-lat="-30.028145" data-long="-51.221927" data-main="yes" data-text="UNIFICADO">
				<span class="location" data-lat="-30.028145" data-long="-51.221927" data-text="UNIFICADO"></span>
			</div>
		</div>
	</section>
	<!-- End Google Map -->
	
	<?php include_once 'includes/footer.php'; ?>

	<a href="#0" class="hm_go_top"></a>
</div>
<!-- End wrapper -->

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script>window.jQuery || document.write('<script src="/js/jquery.js"><\/script>')</script>
<script type="text/javascript" src="/js/flexslider-min.js"></script>
<script src="/js/plugins.js"></script>
<script src="/js/isotope.pkgd.min.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3&amp;sensor=true"></script>
<script src="/js/gmaps.js"></script>
<script type="text/javascript" src="/js/progressbar.min.js"></script>
<!-- this is where we put our custom functions -->
<script type="text/javascript" src="/js/functions.js"></script>
</body>
</html>