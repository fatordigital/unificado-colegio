<?php
$acao_busca_id=mysql_query("select * from conheca where id='4'") or die (mysql_error());
while($r_busca_id=mysql_fetch_array($acao_busca_id)) 
	{
	$texto1=utf8_encode($r_busca_id['texto1']);
	$texto2=utf8_encode($r_busca_id['texto2']);
	$texto3=utf8_encode($r_busca_id['texto3']);
	$texto4=utf8_encode($r_busca_id['texto4']);
	}
?>
<!doctype html>
<!--[if lt IE 7 ]> <html class="ie ie6 ie-lt10 ie-lt9 ie-lt8 ie-lt7 no-js" lang="en"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie ie7 ie-lt10 ie-lt9 ie-lt8 no-js" lang="en"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie ie8 ie-lt10 ie-lt9 no-js" lang="en"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie ie9 ie-lt10 no-js" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--><html class="no-js" lang="en"><!--<![endif]-->
<!-- the "no-js" class is for Modernizr. --> 
<head>

	<title>Unificado Med</title>

    <meta http-equiv="content-type" content="text/html; charset=utf-8" />    
    <meta itemprop="name" content="Unificado Med">
	<meta name="title" content="Unificado Med" />
	<meta http-equiv="content-language" content="pt-br" />
	<meta http-equiv="cache-control" content="no-cache" />
	<meta http-equiv="pragma" content="no-store" />
	<meta http-equiv="refresh" content="none" />
	<meta name="reply-to" content="contato@unificado.com.br">
	<meta name="generator" content="Adobe Dreamweaver Macromedia 6.0">
    <meta itemprop="description" content="As escolhas sempre quando bem feitas, resultam em grandes conquistas. O Unificado Z teve seu início com o antigo Pré-Vestibular, em 1977, formado por um grupo de professores que acreditaram na ideia de oferecer um ensino baseado em um método pedagógico diferente.">
    <meta name="keywords" content="plano empresarial claro, plano corporativo claro, plano claro empresarial, plano claro corporativo, plano empresa, plano corporativo, plano empresarial de telefonia, plano corporativo de telefonia" />
    <meta itemprop="image" content="http://www.unificadomed.com.br/img/meta-imagem.jpg">
	<meta name="abstract" content="As escolhas sempre quando bem feitas, resultam em grandes conquistas. O Unificado Z teve seu início com o antigo Pré-Vestibular, em 1977, formado por um grupo de professores que acreditaram na ideia de oferecer um ensino baseado em um método pedagógico diferente.">    
	<meta name="author" content="WE MAKE | Marketing Digital" />
	<meta name="robots" content="index, follow" />
	<meta name="rating" content="general" />
	<meta name="copyright" content="Copyright Unificado Med 2015. All Rights Reserved." />    
    <meta property="og:type" content="website" />
    <meta property="og:url" content="http://www.unificadomed.com.br" />
    <meta property="og:image" content="http://www.unificadomed.com.br/img/meta-imagem.jpg" />
    <meta property="og:title" content="Unificado Med"/>
    <meta property="og:description" content="As escolhas sempre quando bem feitas, resultam em grandes conquistas. O Unificado Z teve seu início com o antigo Pré-Vestibular, em 1977, formado por um grupo de professores que acreditaram na ideia de oferecer um ensino baseado em um método pedagógico diferente.">
    <meta property="og:site_name" content="Unificado Med" />
    <meta property="og:author" content="WE MAKE Marketing Digital" />
	<meta name="Copyright" content="Grupo Unificado" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<!-- concatenate and minify for production -->
	<link rel="stylesheet" href="/css/style.css" type="text/css" media="all" />
	<link rel="stylesheet" href="/css/bootstrap.css" type="text/css" media="all" />
	<link rel="stylesheet" href="/css/icon-fonts.css" type="text/css" media="all" />
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
</head>

<!-- Class ( site_boxed - dark - preloader1 - preloader2 - preloader3 - light_header - dark_sup_menu - menu_button_mode - transparent_header - header_on_side ) -->
<body class="preloader3">

<div id="main_wrapper">

	<?php include_once 'includes/header.php'; ?>
		
	<!-- Page Title -->

	<section class="content_section page_title">

		<div class="content clearfix">

			<h1 class="">Colégio</h1>
			<div class="breadcrumbs">
				<a href="/">Home</a>
				<span class="crumbs-spacer"><i class="ico-angle-right"></i></span>
				<span>Colégio</span>
			</div>
		</div>

	</section>

	<!-- End Page Title -->

	
	<!-- Icon Boxes -->

	<section class="content_section bg_gray border_b_n">
	        <div class="container row_spacer">
			<div class="main_title centered upper">
				<h2><span class="line"></span>Conheça o Grupo</h2>
			</div>
			<div class="icon_boxes_con style2 reflection clearfix medium_icon clearfix">
				<div class="col-md-3">
					<div class="service_box clearfix">
					<span class="icon circle"><i class="ico-check"></i></span>
					<div class="service_box_con">
						<h3>Quem Somos</h3>
					    <span class="desc"><?php echo $texto1; ?></span>
					    <a href="/colegio/quem-somos" class="ser-box-link"><span></span>Continuar lendo</a>
					</div>
				    </div>
				</div>
				<div class="col-md-3">
					<div class="service_box clearfix">
					<span class="icon circle"><i class="ico-check"></i></span>
					<div class="service_box_con">
						<h3>Histórico</h3>
					    <span class="desc"><?php echo $texto2; ?></span>
					    <a href="/colegio/historico" class="ser-box-link"><span></span>Continuar lendo</a>
					</div>
				    </div>
				</div>
				<div class="col-md-3">
					<div class="service_box clearfix">
					<span class="icon circle"><i class="ico-check"></i></span>
					<div class="service_box_con">
						<h3>Sedes</h3>
					    <span class="desc"><?php echo $texto3; ?></span>
					    <a href="/colegio/sedes" class="ser-box-link"><span></span>Continuar lendo</a>
					</div>
				    </div>
				</div>
				<div class="col-md-3">
					<div class="service_box clearfix">
					<span class="icon circle"><i class="ico-check"></i></span>
					<div class="service_box_con">
						<h3>Grupo Unificado</h3>
					    <span class="desc"><?php echo $texto4; ?></span>
					    <a href="http://www.unificado.com.br" target="_blank" class="ser-box-link"><span></span>Visitar o site</a>
					</div>
				    </div>
				</div>
			</div>
		</div>
	</section>

	<!-- End Icon Boxes -->
    	
	<?php include_once 'includes/footer.php'; ?>

	<a href="#0" class="hm_go_top"></a>
</div>
<!-- End wrapper -->

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script>window.jQuery || document.write('<script src="/js/jquery.js"><\/script>')</script>
<script src="/js/plugins.js"></script>
<script type="text/javascript" src="/js/functions.js"></script>
</body>
</html>