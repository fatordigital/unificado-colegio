<?php
$cto_acao_mail5=mysql_query("select * from form_configurar where id='8'");
while($cto_r_mail5=mysql_fetch_array($cto_acao_mail5)) 
	{
	$info_texto_apoio=utf8_encode($cto_r_mail5['texto_apoio']);
	$info_texto_apoio=str_replace("<ul>","<ul class=\"list3 clearfix\">",$info_texto_apoio);
	}
?>
<!doctype html>
<!--[if lt IE 7 ]> <html class="ie ie6 ie-lt10 ie-lt9 ie-lt8 ie-lt7 no-js" lang="en"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie ie7 ie-lt10 ie-lt9 ie-lt8 no-js" lang="en"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie ie8 ie-lt10 ie-lt9 no-js" lang="en"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie ie9 ie-lt10 no-js" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--><html class="no-js" lang="en"><!--<![endif]-->
<!-- the "no-js" class is for Modernizr. --> 
<head>

	<title>Matrículas</title>

	
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />    
    <meta itemprop="name" content="Unificado Z">
	<meta name="title" content="Matrículas" />
	<meta http-equiv="content-language" content="pt-br" />
	<meta http-equiv="cache-control" content="no-cache" />
	<meta http-equiv="pragma" content="no-store" />
	<meta http-equiv="refresh" content="none" />
	<meta name="reply-to" content="contato@unificado.com.br">
	<meta name="generator" content="Adobe Dreamweaver Macromedia 6.0">
    <meta itemprop="description" content="As escolhas sempre quando bem feitas, resultam em grandes conquistas. O Unificado Z teve seu início com o antigo Pré-Vestibular, em 1977, formado por um grupo de professores que acreditaram na ideia de oferecer um ensino baseado em um método pedagógico diferente.">
    <meta name="keywords" content="plano empresarial claro, plano corporativo claro, plano claro empresarial, plano claro corporativo, plano empresa, plano corporativo, plano empresarial de telefonia, plano corporativo de telefonia" />
    <meta itemprop="image" content="http://www.unificadomed.com.br/img/meta-imagem.jpg">
	<meta name="abstract" content="As escolhas sempre quando bem feitas, resultam em grandes conquistas. O Unificado Z teve seu início com o antigo Pré-Vestibular, em 1977, formado por um grupo de professores que acreditaram na ideia de oferecer um ensino baseado em um método pedagógico diferente.">    
	<meta name="author" content="WE MAKE | Marketing Digital" />
	<meta name="robots" content="index, follow" />
	<meta name="rating" content="general" />
	<meta name="copyright" content="Copyright Grupo Unificado 2015. All Rights Reserved." />    
    <meta property="og:type" content="website" />
    <meta property="og:url" content="http://www.unificadomed.com.br/matriculas" />
    <meta property="og:image" content="http://www.unificadomed.com.br/img/meta-imagem.jpg" />
    <meta property="og:title" content="Matrículas"/>
    <meta property="og:description" content="As escolhas sempre quando bem feitas, resultam em grandes conquistas. O Unificado Z teve seu início com o antigo Pré-Vestibular, em 1977, formado por um grupo de professores que acreditaram na ideia de oferecer um ensino baseado em um método pedagógico diferente.">
    <meta property="og:site_name" content="Unificado Med" />
    <meta property="og:author" content="WE MAKE Marketing Digital" />
	<meta name="Copyright" content="Grupo Unificado" />
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- concatenate and minify for production -->
	<link rel="stylesheet" href="/css/style.css" type="text/css" media="all" />
	<link rel="stylesheet" href="/css/bootstrap.css" type="text/css" media="all" />
	<link rel="stylesheet" href="/css/magnific-popup.css" type="text/css" media="all" />
	<link rel="stylesheet" href="/css/icon-fonts.css" type="text/css" media="all" />
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
</head>

<!-- Class ( site_boxed - dark - preloader1 - preloader2 - preloader3 - light_header - dark_sup_menu - menu_button_mode - transparent_header - header_on_side ) -->
<body class="preloader3">

<div id="main_wrapper">

	<?php include_once 'includes/header.php'; ?>
		
	<!-- Page Title -->

	<section class="content_section page_title">

		<div class="content clearfix">

			<h1 class="">Matrículas</h1>
			<div class="breadcrumbs">
				<a href="/">Home</a>
				<span class="crumbs-spacer"><i class="ico-angle-right"></i></span>
				<span>Matrículas</span>
			</div>
		</div>

	</section>

	<!-- End Page Title -->

	
	<!-- Contact Us -->
	<section class="content_section">
		<div class="content row_spacer no_padding">	
			<div class="rows_container clearfix">
				<div class="col-md-7">
					<?php if (isset($sucesso_contato) && $sucesso_contato == 1) { ?><h4 style="margin-top:0px; padding-top:0px; margin-bottom:60px;">Mensagem enviada com sucesso!</h4><?php } ?>
                <?php if (isset($erro_form) && $erro_form > 0) { ?><h4 style="margin-top:0px; padding-top:0px; margin-bottom:60px; color:red">Preencha os campos obrigatórios!</h4><?php } ?>
                
					
					<h2 class="title1 upper">Agende entrevista com a Equipe</h2>
                    <br>
					<form action="/matriculas" method="post" name="matriculas" id="matriculas"> 
						<div class="form_row clearfix">
							<label for="contact-us-name">
								<span class="hm_field_name"><?php if (isset($erro_nome) && $erro_nome == 1) { echo"<font style='color:red'>"; } ?>Nome<?php if (isset($erro_nome) && $erro_nome == 1) { echo"</font>"; } ?></span>
								<span class="hm_requires_star">*</span>
							</label>
							<input class="form_fill_fields hm_input_text" type="text" name="nome" id="nome" placeholder="Nome" value="<?php if (isset($erro_form) && $erro_form == 1) { echo $nome; } ?>">
						</div>
						<div class="form_row clearfix">
							<label for="contact-us-mail">
								<span class="hm_field_name"><?php if ((isset($erro_email) && $erro_email == 1) or (isset($erro_valida_email) && $erro_valida_email == 1)) { echo"<font style='color:red'>"; } ?>Email<?php if ((isset($erro_email) && $erro_email == 1) or (isset($erro_valida_email) && $erro_valida_email == 1)) { echo"</font>"; } ?></span>
								<span class="hm_requires_star">*</span>
							</label>
							<input class="form_fill_fields hm_input_text" type="email" name="email" id="email" placeholder="mail@nomedosite.com" value="<?php if (isset($erro_form) && $erro_form == 1) { echo $email; } ?>">
						</div>
						<div class="form_row clearfix">
							<label for="contact-us-phone">
								<span class="hm_field_name">Telefone</span>
							</label>
							<input class="form_fill_fields hm_input_text" type="text" name="telefone" id="telefone" placeholder="Telefone" value="<?php if (isset($erro_form) && $erro_form == 1) { echo $telefone; } ?>">
						</div>
						<div class="form_row clearfix">
							<label for="contact-us-message">
								<span class="hm_field_name"><?php if (isset($erro_email) && $erro_email == 1) { echo"<font style='color:red'>"; } ?>Comentários<?php if (isset($erro_comentario) && $erro_comentario == 1) { echo"</font>"; } ?></span>
								<span class="hm_requires_star">*</span>
							</label>
							<textarea class="form_fill_fields hm_textarea" name="comentario" id="comentario" placeholder="Comentários"><?php if (isset($erro_form) && $erro_form == 1) { echo $comentario; } ?></textarea>
						</div>
						<div class="form_row clearfix">
							<button type="submit" class="send_button full_button" name="contact-us-submit" id="contact-us-submit" value="submit">
								<i class="ico-check3"></i>
								<span>Enviar Mensagem</span>
							</button>
						</div>
                    <input type="hidden" name="enviar_matricula" value="on">
					</form>
				</div><!-- Grid -->

				<div class="col-md-5">

					<h2 class="title1 upper">Documentos Necessários</h2><br>
                    
                    <?php echo $info_texto_apoio; ?>

				</div>



				</div><!-- Grid -->
			</div>
		</div>
	</section>
	<!-- End Contact Us -->
        	
	<?php include_once 'includes/footer.php'; ?>

	<a href="#0" class="hm_go_top"></a>
</div>
<!-- End wrapper -->

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script>window.jQuery || document.write('<script src="/js/jquery.js"><\/script>')</script>
<script src="/js/plugins.js"></script>
<!-- this is where we put our custom functions -->
<script type="text/javascript" src="/js/functions.js"></script>
</body>
</html>