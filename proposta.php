<?php
$acao_busca_id=mysql_query("select * from proposta_introducao where id='4'") or die (mysql_error());
while($r_busca_id=mysql_fetch_array($acao_busca_id)) 
    {
    //$rec_titulo=utf8_encode($r_busca_id['titulo']);
    //$rec_subtitulo=utf8_encode($r_busca_id['subtitulo']);
    //$rec_imagem=$r_busca_id['imagem'];
    $intro_texto=utf8_encode($r_busca_id['texto']);
    }

$acao_busca_id=mysql_query("select * from proposta_chamadas where id='4'") or die (mysql_error());
while($r_busca_id=mysql_fetch_array($acao_busca_id)) 
    {
    //$rec_titulo=utf8_encode($r_busca_id['titulo']);
    //$rec_subtitulo=utf8_encode($r_busca_id['subtitulo']);
    //$rec_imagem=$r_busca_id['imagem'];
    $rec_texto1=utf8_encode($r_busca_id['texto1']);
    $rec_texto2=utf8_encode($r_busca_id['texto2']);
    }
?>
<!doctype html>
<!--[if lt IE 7 ]> <html class="ie ie6 ie-lt10 ie-lt9 ie-lt8 ie-lt7 no-js" lang="en"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie ie7 ie-lt10 ie-lt9 ie-lt8 no-js" lang="en"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie ie8 ie-lt10 ie-lt9 no-js" lang="en"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie ie9 ie-lt10 no-js" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--><html class="no-js" lang="en"><!--<![endif]-->
<!-- the "no-js" class is for Modernizr. --> 
<head>

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	
	<!-- Important stuff for SEO, don't neglect. (And don't dupicate values across your site!) -->
	<title>Proposta Pedagógica</title>
	<meta name="author" content="" />
	<meta name="description" content="" />
	
	<!-- Don't forget to set your site up: http://google.com/webmasters -->
	<meta name="google-site-verification" content="" />
	<meta name="Copyright" content="" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<!-- Use Iconifyer to generate all the favicons and touch icons you need: http://iconifier.net -->
	<link rel="shortcut icon" href="/images/favicon/favicon.ico" type="image/x-icon" />
	<link rel="apple-touch-icon" href="/images/favicon/apple-touch-icon.png" />
	<link rel="apple-touch-icon" sizes="57x57" href="/images/favicon/apple-touch-icon-57x57.png" />
	<link rel="apple-touch-icon" sizes="72x72" href="/images/favicon/apple-touch-icon-72x72.png" />
	<link rel="apple-touch-icon" sizes="76x76" href="/images/favicon/apple-touch-icon-76x76.png" />
	<link rel="apple-touch-icon" sizes="114x114" href="/images/favicon/apple-touch-icon-114x114.png" />
	<link rel="apple-touch-icon" sizes="120x120" href="/images/favicon/apple-touch-icon-120x120.png" />
	<link rel="apple-touch-icon" sizes="144x144" href="/images/favicon/apple-touch-icon-144x144.png" />
	<link rel="apple-touch-icon" sizes="152x152" href="/images/favicon/apple-touch-icon-152x152.png" />
	
	<!-- concatenate and minify for production -->
	<link rel="stylesheet" href="/css/style.css" type="text/css" media="all" />
	<link rel="stylesheet" href="/css/flexslider.css" type="text/css" media="all" />
	<link rel="stylesheet" href="/css/bootstrap.css" type="text/css" media="all" />
	<link rel="stylesheet" href="/css/animate.min.css" type="text/css" media="all" />
	
	<link rel="stylesheet" href="/css/magnific-popup.css" type="text/css" media="all" />
	<link rel="stylesheet" href="/css/icon-fonts.css" type="text/css" media="all" />

	<link href='http://fonts.googleapis.com/css?family=Oswald:400,700,300' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Lato:300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
</head>

<!-- Class ( site_boxed - dark - preloader1 - preloader2 - preloader3 - light_header - dark_sup_menu - menu_button_mode - transparent_header - header_on_side ) -->
<body class="preloader3">

<div id="main_wrapper">

	<?php include_once 'includes/header.php'; ?>
		
	<!-- Page Title -->

	<section class="content_section page_title">

		<div class="content clearfix">

			<h1 class="">Proposta Pedagógica</h1>
			<div class="breadcrumbs">
				<a href="/">Home</a>
				<span class="crumbs-spacer"><i class="ico-angle-right"></i></span>
				<span>Proposta Pedagógica</span>
			</div>
		</div>

	</section>

	<!-- End Page Title -->

	
	<!-- Icon Boxes -->

	<section class="content_section bg_gray border_b_n">
	        <div class="container row_spacer">
			<div class="main_title centered upper">
				<?php echo $intro_texto; ?>
                <hr><br>
			</div>
			<div class="icon_boxes_con style2 reflection clearfix medium_icon clearfix">
				<div class="col-md-6">
					<div class="service_box clearfix">
					<span class="icon circle"><i class="ico-check"></i></span>
					<div class="service_box_con">
						<h3>Projeto Pedagógico</h3>
					    <span class="desc"><?php echo $rec_texto1; ?></span>
					    <a href="/proposta/projeto-pedagogico" class="ser-box-link"><span></span>Continuar lendo</a>
					</div>
				    </div>
				</div>
				<div class="col-md-6">
					<div class="service_box clearfix">
					<span class="icon circle"><i class="ico-check"></i></span>
					<div class="service_box_con">
						<h3>Processo Avaliativo</h3>
					    <span class="desc"><?php echo $rec_texto2; ?></span>
					    <a href="/proposta/processo-avaliativo" class="ser-box-link"><span></span>Continuar lendo</a>
					</div>
				    </div>
				</div>
			</div>
		</div>
	</section>

	<!-- End Icon Boxes -->
	
	<?php include_once 'includes/footer.php'; ?>

	<a href="#0" class="hm_go_top"></a>
</div>
<!-- End wrapper -->

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script>window.jQuery || document.write('<script src="/js/jquery.js"><\/script>')</script>
<script type="text/javascript" src="/js/flexslider-min.js"></script>
<script src="/js/plugins.js"></script>
<script src="/js/isotope.pkgd.min.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3&amp;sensor=true"></script>
<script src="/js/gmaps.js"></script>
<script type="text/javascript" src="/js/progressbar.min.js"></script>
<!-- this is where we put our custom functions -->
<script type="text/javascript" src="/js/functions.js"></script>
</body>
</html>