<?php
// funcao para pegar dimensoes em pixel da uma imgem

function DimensaoImagem ($imagem, $caminho, $qual)
	{
    $filename_p = $caminho."/".$imagem; 
    $imagesize_p = @getimagesize($filename_p); 
    if ($qual == "x")
    	{
       	$x_p = $imagesize_p[0]; // x ser&aacute; a largura.
       	$retorno = $x_p;
       	}
   	if ($qual == "y")
    	{
        $y_p = $imagesize_p[1]; // y ser&aacute; a altura.
       	$retorno = $y_p;
       	}
    $retorno = ceil($retorno);
    
    return $retorno;
   	}
?>