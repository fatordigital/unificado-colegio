<?php
// funcao que retorna se qual o tipo de cliente e em 4 formatos diferentes

function ClienteTipoCadastro ($tipocliente, $tiporesposta)
	{
	// tipocliente - tipocadastro (1 a 7)
	// tiporesposta eh como deseja a nomenclatura (singular, caixa alta, etc)
	// tiporesposta 1 - caixa alta plural
	// tiporesposta 2 - caixa alta singular
	// tiporesposta 3 - caixa baixa plural
	// tiporesposta 3 - caixa baixa singular
	
	global $ativar_base_cliente_nomenclatura1;
	global $ativar_base_cliente_nomenclatura2;
	
	global $ativar_base_fornecedor_nomenclatura1;
	global $ativar_base_fornecedor_nomenclatura2;
	
	global $ativar_base_colaborador_nomenclatura1;
	global $ativar_base_colaborador_nomenclatura2;
	
	global $ativar_base_acionista_nomenclatura1;
	global $ativar_base_acionista_nomenclatura2;
	
	global $ativar_base_parceiro_nomenclatura1;
	global $ativar_base_parceiro_nomenclatura2;
	
	global $ativar_base_investidor_nomenclatura1;
	global $ativar_base_investidor_nomenclatura2;
	
	global $ativar_base_filial_nomenclatura1;
	global $ativar_base_filial_nomenclatura2;
	
	if ($tipocliente == 1)
		{
		//$nome1 = "CLIENTES";
		//$nome2 = "CLIENTE";
		//$nome3 = "Cliente";
		//$nome4 = "Clientes";
		if ($ativar_base_cliente_nomenclatura1 != "")
			{
			$nome3 = $ativar_base_cliente_nomenclatura1;
			$nome2 = maiusculo($ativar_base_cliente_nomenclatura1);
			}
		else
			{
			$nome2 = "CLIENTE";
			$nome3 = "Cliente";
			}
		if ($ativar_base_cliente_nomenclatura2 != "")
			{
			$nome4 = $ativar_base_cliente_nomenclatura2;
			$nome1 = minusculo($ativar_base_cliente_nomenclatura2);
			}
		else
			{
			$nome1 = "CLIENTES";
			$nome4 = "Clientes";
			}
		}
		
	if ($tipocliente == 2)
		{
		if ($ativar_base_fornecedor_nomenclatura1 != "")
			{
			$nome3 = $ativar_base_fornecedor_nomenclatura1;
			$nome2 = maiusculo($ativar_base_fornecedor_nomenclatura1);
			}
		else
			{
			$nome2 = "FORNECEDOR";
			$nome3 = "Fornecedor";
			}
		if ($ativar_base_fornecedor_nomenclatura2 != "")
			{
			$nome4 = $ativar_base_fornecedor_nomenclatura2;
			$nome1 = minusculo($ativar_base_fornecedor_nomenclatura2);
			}
		else
			{
			$nome1 = "FORNECEDORES";
			$nome4 = "Fornecedores";
			}
		}
	if ($tipocliente == 3)
		{
		if ($ativar_base_colaborador_nomenclatura1 != "")
			{
			$nome3 = $ativar_base_colaborador_nomenclatura1;
			$nome2 = maiusculo($ativar_base_colaborador_nomenclatura1);
			}
		else
			{
			$nome2 = "COLABORADOR";
			$nome3 = "Colaborador";
			}
		if ($ativar_base_colaborador_nomenclatura2 != "")
			{
			$nome4 = $ativar_base_colaborador_nomenclatura2;
			$nome1 = minusculo($ativar_base_colaborador_nomenclatura2);
			}
		else
			{
			$nome1 = "COLABORADORES";
			$nome4 = "Colaboradores";
			}
		}
	if ($tipocliente == 4)
		{
		if ($ativar_base_acionista_nomenclatura1 != "")
			{
			$nome3 = $ativar_base_acionista_nomenclatura1;
			$nome2 = maiusculo($ativar_base_acionista_nomenclatura1);
			}
		else
			{
			$nome2 = "ACIONISTA";
			$nome3 = "Acionista";
			}
		if ($ativar_base_acionista_nomenclatura2 != "")
			{
			$nome4 = $ativar_base_acionista_nomenclatura2;
			$nome1 = minusculo($ativar_base_acionista_nomenclatura2);
			}
		else
			{
			$nome1 = "ACIONISTAS";
			$nome4 = "Acionistas";
			}
		}
	if ($tipocliente == 5)
		{
		if ($ativar_base_parceiro_nomenclatura1 != "")
			{
			$nome3 = $ativar_base_parceiro_nomenclatura1;
			$nome2 = maiusculo($ativar_base_parceiro_nomenclatura1);
			}
		else
			{
			$nome2 = "PARCEIRO";
			$nome3 = "Parceiro";
			}
		if ($ativar_base_parceiro_nomenclatura2 != "")
			{
			$nome4 = $ativar_base_parceiro_nomenclatura2;
			$nome1 = minusculo($ativar_base_parceiro_nomenclatura2);
			}
		else
			{
			$nome1 = "PARCEIROS";
			$nome4 = "Parceiros";
			}
		}
	if ($tipocliente == 6)
		{
		if ($ativar_base_investidor_nomenclatura1 != "")
			{
			$nome3 = $ativar_base_investidor_nomenclatura1;
			$nome2 = maiusculo($ativar_base_investidor_nomenclatura1);
			}
		else
			{
			$nome2 = "INVESTIDOR";
			$nome3 = "Investidor";
			}
		if ($ativar_base_investidor_nomenclatura2 != "")
			{
			$nome4 = $ativar_base_investidor_nomenclatura2;
			$nome1 = minusculo($ativar_base_investidor_nomenclatura2);
			}
		else
			{
			$nome1 = "INVESTIDORES";
			$nome4 = "Investidores";
			}
		}
	if ($tipocliente == 7)
		{
		if ($ativar_base_filial_nomenclatura1 != "")
			{
			$nome3 = $ativar_base_filial_nomenclatura1;
			$nome2 = maiusculo($ativar_base_filial_nomenclatura1);
			}
		else
			{
			$nome2 = "FILIAL";
			$nome3 = "Filial";
			}
		if ($ativar_base_filial_nomenclatura2 != "")
			{
			$nome4 = $ativar_base_filial_nomenclatura2;
			$nome1 = minusculo($ativar_base_filial_nomenclatura2);
			}
		else
			{
			$nome1 = "FILIAIS";
			$nome4 = "Filiais";
			}
		}
	
	if ($tiporesposta == 1) return $nome1;
	if ($tiporesposta == 2) return $nome2;
	if ($tiporesposta == 3) return $nome3;
	if ($tiporesposta == 4) return $nome4;
	}

function ClienteTipoGenero ($tipocliente)
	{
	global $ativar_base_fornecedor_nomenclatura_genero;
	global $ativar_base_colaborador_nomenclatura_genero;
	global $ativar_base_acionista_nomenclatura_genero;
	global $ativar_base_parceiro_nomenclatura_genero;
	global $ativar_base_investidor_nomenclatura_genero;
	global $ativar_base_filial_nomenclatura_genero;
	
	// genero = 0 MASCULINO
	// genero = 1 FEMININO
	
	if ($tipocliente == 1)
		{
		$genero = 0;
		}
		
	if ($tipocliente == 2)
		{
		if ($ativar_base_fornecedor_nomenclatura_genero == 1)
			{
			$genero = 1;
			}
		else
			{
			$genero = 0;
			}
		}
	if ($tipocliente == 3)
		{
		if ($ativar_base_colaborador_nomenclatura_genero == 1)
			{
			$genero = 1;
			}
		else
			{
			$genero = 0;
			}
		}
	if ($tipocliente == 4)
		{
		if ($ativar_base_acionista_nomenclatura_genero == 1)
			{
			$genero = 1;
			}
		else
			{
			$genero = 0;
			}
		}
	if ($tipocliente == 5)
		{
		if ($ativar_base_parceiro_nomenclatura_genero == 1)
			{
			$genero = 1;
			}
		else
			{
			$genero = 0;
			}
		}
	if ($tipocliente == 6)
		{
		if ($ativar_base_investidor_nomenclatura_genero == 1)
			{
			$genero = 1;
			}
		else
			{
			$genero = 0;
			}
		}
	if ($tipocliente == 7)
		{
		if ($ativar_base_filial_nomenclatura_genero == 1)
			{
			$genero = 1;
			}
		else
			{
			$genero = 0;
			}
		}
	
	return $genero;
	}
?>