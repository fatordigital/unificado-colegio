<?php
// Fun��o para transformar strings em Mai�scula ou Min�scula com acentos
// $palavra = a string propriamente dita
// $tp = tipo da convers�o: 1 para mai�sculas e 0 para min�sculas
function CaixaAltaCaixaBaixa($term, $tp) {
    if ($tp == "1") $palavra = strtr(strtoupper($term),"������������������������������","������������������������������");
    elseif ($tp == "0") $palavra = strtr(strtolower($term),"������������������������������","������������������������������");
    return $palavra;
}

// Exemplo de Utiliza��o - Mai�scula
//$exemplo1 = "not�cias";
//echo CaixaAltaCaixaBaixa($exemplo1, 1);

// Exemplo de Utiliza��o - Min�scula
//$exemplo2 = "NOT�CIAS";
//echo CaixaAltaCaixaBaixa($exemplo2, 0);
?>