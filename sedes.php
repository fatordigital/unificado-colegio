<!doctype html>
<!--[if lt IE 7 ]> <html class="ie ie6 ie-lt10 ie-lt9 ie-lt8 ie-lt7 no-js" lang="en"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie ie7 ie-lt10 ie-lt9 ie-lt8 no-js" lang="en"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie ie8 ie-lt10 ie-lt9 no-js" lang="en"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie ie9 ie-lt10 no-js" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--><html class="no-js" lang="en"><!--<![endif]-->
<!-- the "no-js" class is for Modernizr. --> 
<head>

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	
	<!-- Important stuff for SEO, don't neglect. (And don't dupicate values across your site!) -->
	<title>Sedes</title>
	<meta name="author" content="" />
	<meta name="description" content="" />
	
	<!-- Don't forget to set your site up: http://google.com/webmasters -->
	<meta name="google-site-verification" content="" />
	<meta name="Copyright" content="" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<!-- Use Iconifyer to generate all the favicons and touch icons you need: http://iconifier.net -->
	<link rel="shortcut icon" href="/images/favicon/favicon.ico" type="image/x-icon" />
	<link rel="apple-touch-icon" href="/images/favicon/apple-touch-icon.png" />
	<link rel="apple-touch-icon" sizes="57x57" href="/images/favicon/apple-touch-icon-57x57.png" />
	<link rel="apple-touch-icon" sizes="72x72" href="/images/favicon/apple-touch-icon-72x72.png" />
	<link rel="apple-touch-icon" sizes="76x76" href="/images/favicon/apple-touch-icon-76x76.png" />
	<link rel="apple-touch-icon" sizes="114x114" href="/images/favicon/apple-touch-icon-114x114.png" />
	<link rel="apple-touch-icon" sizes="120x120" href="/images/favicon/apple-touch-icon-120x120.png" />
	<link rel="apple-touch-icon" sizes="144x144" href="/images/favicon/apple-touch-icon-144x144.png" />
	<link rel="apple-touch-icon" sizes="152x152" href="/images/favicon/apple-touch-icon-152x152.png" />
	
	<!-- concatenate and minify for production -->
	<link rel="stylesheet" href="/css/style.css" type="text/css" media="all" />
	<link rel="stylesheet" href="/css/flexslider.css" type="text/css" media="all" />
	<link rel="stylesheet" href="/css/bootstrap.css" type="text/css" media="all" />
	<link rel="stylesheet" href="/css/animate.min.css" type="text/css" media="all" />
	
	<link rel="stylesheet" href="/css/magnific-popup.css" type="text/css" media="all" />
	<link rel="stylesheet" href="/css/icon-fonts.css" type="text/css" media="all" />

	<link href='http://fonts.googleapis.com/css?family=Oswald:400,700,300' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Lato:300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
</head>

<!-- Class ( site_boxed - dark - preloader1 - preloader2 - preloader3 - light_header - dark_sup_menu - menu_button_mode - transparent_header - header_on_side ) -->
<body class="preloader3">

<div id="main_wrapper">

	<?php include_once 'includes/header.php'; ?>
		
	<!-- Page Title -->

	<section class="content_section page_title">

		<div class="content clearfix">

			<h1 class="">Sedes</h1>
			<div class="breadcrumbs">
				<a href="/">Home</a>
				<span class="crumbs-spacer"><i class="ico-angle-right"></i></span>
				<a href="/colegio">Colégio</a>
				<span class="crumbs-spacer"><i class="ico-angle-right"></i></span>
				<span>Sedes</span>
			</div>
		</div>

	</section>

	<!-- End Page Title -->
	
	<!-- Intro Banner -->
	<section class="content_section">
		<div class="container row_spacer2">
			<div class="container">
				<div class="content clearfix">
                    <div class="col-md-8">
                    <?php
					$i = 0;
                    $it = 0;
					$acao=mysql_query("select * from empresa_sedes where site4='1' order by posicao asc,id asc") or die (mysql_error());
					$conta=mysql_num_rows($acao);
					if ($conta > 0)
						{
						while($r=mysql_fetch_array($acao)) 
							{
							$rec_titulo=utf8_encode($r['titulo']);
							$rec_subtitulo=utf8_encode($r['subtitulo']);
							$rec_subtitulo2=utf8_encode($r['subtitulo2']);
							$rec_subtitulo3=utf8_encode($r['subtitulo3']);
							
							$i = $i + 1;
							$it = $it + 1;
							?>
                        	<div class="col-md-6">
                            <div class="contact_details_row clearfix">
                                <span class="icon">
                                    <i class="ico-location5"></i>
                                </span>
                                <div class="c_con">
                                    <span class="c_title"><?php echo $rec_titulo; ?></span>
                                    <span class="c_detail">
                                        <span class="c_name"><?php echo $rec_subtitulo; ?></span>
                                        <span class="c_name"><?php echo $rec_subtitulo2; ?></span>
                                        <span class="c_desc"><a href="mailto:<?php echo $rec_subtitulo3; ?>"><?php echo $rec_subtitulo3; ?></a></span>
                                    </span>
                                </div>
                            </div>
							</div>
                            <?php
							if ($i == 2)
								{
								?>
		                        <div class="clearfix"></div>
        		                <div class="line" style="padding:15px"></div>
                                <?php
								$i = 0;
								}
							
							if ($i == 1)
								{
								?>
                                <div class="col-md-6">
                                </div>
                                <?php
								}
							}
						}
					?>

                	</div>
                    <div class="col-md-4">                    
                    
                        <div id="fb-root"></div>
                        <script>(function(d, s, id) {
                          var js, fjs = d.getElementsByTagName(s)[0];
                          if (d.getElementById(id)) return;
                          js = d.createElement(s); js.id = id;
                          js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.5";
                          fjs.parentNode.insertBefore(js, fjs);
                        }(document, 'script', 'facebook-jssdk'));</script>
                        
                        <div class="fb-page" data-href="https://www.facebook.com/ColegioUnificadoCentro" data-tabs="timeline" data-height="470" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/ColegioUnificadoCentro"><a href="https://www.facebook.com/ColegioUnificadoCentro">Colégio Unificado Centro</a></blockquote></div></div>

                        <!-- Gallery 
                        <div class="thumbs_gall_slider_con content_thumbs_gall gall_arrow2 clearfix">
                            <div class="thumbs_gall_slider_larg owl-carousel">
                                <div class="item">
                                    <a href="/images/blog/blog1.jpg">
                                        <img src="/images/blog/blog1.jpg" alt="Image Title">
                                    </a>
                                </div>
                                <div class="item">
                                    <a href="/images/blog/blog1.jpg">
                                        <img src="/images/blog/blog1.jpg" alt="Image Title">
                                    </a>
                                </div>
                                <div class="item">
                                    <a href="/images/blog/blog1.jpg">
                                        <img src="/images/blog/blog1.jpg" alt="Image Title">
                                    </a>
                                </div>
                            </div>
                        </div>
                        -->
                    </div>
				</div>
			</div>
		</div>
	</section>
	<!-- End Intro Banner -->
	
	<?php include_once 'includes/footer.php'; ?>

	<a href="#0" class="hm_go_top"></a>
</div>
<!-- End wrapper -->

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script>window.jQuery || document.write('<script src="/js/jquery.js"><\/script>')</script>
<script type="text/javascript" src="/js/flexslider-min.js"></script>
<script src="/js/plugins.js"></script>
<script src="/js/isotope.pkgd.min.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3&amp;sensor=true"></script>
<script src="/js/gmaps.js"></script>
<script type="text/javascript" src="/js/progressbar.min.js"></script>
<!-- this is where we put our custom functions -->
<script type="text/javascript" src="/js/functions.js"></script>
</body>
</html>